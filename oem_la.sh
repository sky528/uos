#!/bin/bash
#SN file
function uos_sn(){
    [ -f $SN_KEYS_FILE ] && rm $SN_KEY_FILE 
    # 定义包含 SN 对应表的字符串
    uos_sn_str=$(cat <<EOF
1234567890123
2345678901234
EOF
)
    # 将字符串转换为数组
    IFS=$'\n' read -d '' -ra uos_sn_array <<< "$uos_sn_str"
    # 遍历数组处理 SN
    for line in "${_array[@]}"; do
      sn=$(echo "$line" | awk '{print $1}')
     echo "$sn" >>$SN_KEYS_FILE
    done
}

uos_sn