#!/bin/bash
# 2024/09/06,增加参数控制,增加网络更新。
# 2024/09/03,更改配置文件为config.sh，更改部分文件路径，增加日志自动删除。
# 2024/09/01,增加配置文件。

#定义变量
scrip_update=2024/09/06
passwd="QWRtaW4jMTIz"
developer_mode_enabled=1
script_dir=$(dirname "$(readlink -f "$0")") # 获取脚本目录
parent_dir=$(dirname "$(dirname "$(readlink -f "$0")")") # 获取脚本上级目录
sn_list="${parent_dir}/sn_list_keys.txt"
USED_KEYS_FILE="${parent_dir}/used_keys.txt"  #定义已使用的激活码文件
SN_KEYS_FILE="${parent_dir}/Serial_Number.txt" #SN_FILE
config_file="${script_dir}/config.sh"
log_file="${script_dir}/$(date +'%Y-%m-%d').log"
expiry_date="MjAyNDEyMzE="
url_path="aHR0cHM6Ly9yYXcuZ2l0Y29kZS5jb20vc2t5NTI4L3Vvcy9yYXcvbWFpbg=="
clear
# 定义函数
function load_config(){
while IFS='=' read -r key value; do
    # Skip lines starting with # (comments)
    #[[ $key =~ ^# ]] && continue
    [[ "${key:0:1}" == "#" ]] && continue
    export "$key=$value"
done < $config_file
}

function load_config_sh(){
[ -f "${script_dir}/config.sh" ] || log_red "[Error] File not found,exit！！！" 
source "$config_file" && GRE config read ok || exit 12 >>$log_file
}

function sudoo(){
	newpasswd=$(echo -n "$passwd" |base64 -d)
	echo "${newpasswd}" | sudo -S $@
}

function rm_log(){
# 保留最近3天日志文件
all_logs=$(ls -t "${script_dir}"/*.log) >/dev/null 2>&1
log_files=($all_logs)
if [ ${#log_files[@]} -gt 3 ]; then
    logs_to_remove=("${log_files[@]:3}")
    for log_file_del in "${logs_to_remove[@]}"; do
        rm "$log_file_del"
    done
fi
}

function RED(){
  echo -e "\033[31m$@\033[0m"
}

function GRE(){
  echo -e "\033[32m$@\033[0m"
}

function log(){	
    echo "$(date +'%Y-%m-%d %H:%M:%S'): $1" >> $log_file 2>/dev/null
	GRE "$1"	
}

function log_red(){
    echo "$(date +'%Y-%m-%d %H:%M:%S'): $1" >> $log_file 2>/dev/null &
	RED "$1" &
}

function check_root(){
if [ -f "/var/lib/deepin/developer-mode/enabled" ]; then
	log_reg "开发者模式已开启。"
	developer_mode_enabled=1
else
	log_reg "开发者模式未开启。"
	developer_mode_enabled=0
	cp ${script_fun}/signed_open-root_0.0.2_all.deb ~/.openroot.deb || { developer_mode_enabled=0; return; }
	deepin-deb-installer ~/.openroot.deb >/dev/null 2>&1 && developer_mode_enabled=1
fi
}

function open_root(){
if [ $developer_mode_enabled -eq 1 ]; then
	log_red "开发者模式已开启。"
elif [ $developer_mode_enabled -eq 0 ]; then
	log_red "开发者模式未开启,一键打开开发者模式。"
	cp ${script_fun}/signed_open-root_0.0.2_all.deb ~/.openroot.deb || { developer_mode_enabled=0; return; }
	deepin-deb-installer ~/.openroot.deb >/dev/null 2>&1 && developer_mode_enabled=1
 	log_red "开发者模式已开启。"
	clear
fi
}

function disable_developer_mode(){
# 关闭开发者模式
[ -f /var/lib/deepin/developer-mode/enabled ] &&  sudoo rm /var/lib/deepin/developer-mode/enabled  || true
}

function check_elf(){
# 安全中心，允许任意应用
if [ $developer_mode_enabled -eq 1 ];then
	local elf_path=/usr/share/deepin-elf-verify/mode
	sudoo chmod 777 $elf_path
	#cat $elf_path
	sudoo sed -i 's/1/0/g' $elf_path
	sudoo chmod 644 $elf_path
	sudoo systemctl restart deepin-elf-verify
	log_red  "安全中心:允许任意应用"
fi
}

function check_network(){
	local attempts=0
	while [ "$network" -eq 1 ] && [ $attempts -lt 3 ]; do
		log_red "检查网络是否连接"
		ping -c 1 www.baidu.com > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			log "网络已连通"
			return
		else
			log_red "【Error】网络不通，请检查网络后重试。"
			sleep 2
			read -N 1 -p "Press any key to continue..."
			((attempts++))	
		fi
	done
	if [ $network -eq 1 ]; then
        log_red "[Error] 经过 3 次尝试后网络仍然不通，退出。"
        exit 13 >>$log_file
    fi
}

function get_sn(){
	if [ $get_sn_keys -eq 1 ]; then
		machine_sn=$(dmidecode -t 1 | awk "/Serial Number:/{print $NF}" | cut -d ":" -f 2)
		uos_sn=$(uos-activator-cmd -q | awk '/序列号：/{split($0,a,"序列号："); print a[2]}')
		uos_sqdx=$(uos-activator-cmd -q | awk '/授权对象：/{split($0,a,"授权对象："); print a[2]}')
		echo "" >>$sn_list
		echo -n "$uos_sqdx;$machine_sn;$uos_sn" >>$sn_list	
	fi
}

function get_machine_model(){
SYSTEM=$(dmidecode -t 1 | awk "/Version:/{print $NF}" | cut -d ":" -f 2)
if expr "$SYSTEM" : ".*HUAWEI QingYun.*" > /dev/null; then
	case "$SYSTEM" in
		*L540*)
		SYSTEM="华为擎云 L540"
		;;
		*W525*)
		SYSTEM="华为擎云 W525"
	esac
elif expr "$SYSTEM" : ".*W585x.*" > /dev/null; then
	SYSTEM="华为擎云 W585x"
else
	log_red "[Error] 未知型号！"
	SYSTEM="未知型号"
fi
}

function register_info(){	
	current_date=$(date +%Y%m%d)
	expiry_date=$(echo -n $expiry_date | base64 -d)
	if [ "$current_date" -gt "$expiry_date" ];then
		log_red "[Error] 脚本已过期，无法使用"
		sleep 5
		exit 15 >>$log_file
	fi
}

function echoHeadInfo(){
 cat << EOF
+--------------------------------------------------------------+
|                【自动安装软件、激活系统脚本】                |
|                                         By：阿杜 $scrip_update  |
+--------------------------------------------------------------+
|                        $SYSTEM                         |
+--------------------------------------------------------------+
	脚 本 目 录：  $script_dir
	UOS激活码文件：$SN_KEYS_FILE
	配 置 文 件：  $config_file
	日 志 文 件：  $log_file
+--------------------------------------------------------------+
EOF
}

function echo_help(){
cat << EOFF
参数：
-i		install_deb				-a		activate_uos 
-o		open_root				-f		check_elf
-yh		yh_power				-sn		get_sn		
-net		check_network				-job		logo_job				
-update		update_uos				-desktop	desktop_lnk
-run:wps	run_deb					-p:password	password
-end:0 		end_operation	
--helpme	echo_help				--config	script_config
EOFF
}

function script_config(){
[ -f "${script_dir}/config.sh" ] && mv "${script_dir}/config.sh" "${script_dir}/config.sh.bak"
cat > config.sh << EOF
#!/bin/bash
#配置文件
passwd=QWRtaW4jMTIz
developer_mode_enabled=1	# 开发者模式状态，0关闭，1开启
network=1					# 网络检测，0关闭，1开启
uos_update=0				# 更新仓库列表,0关闭，1开启
installer_soft=0			# 安装软件,0关闭，1开启
rundeb=						# 运行软件，软件名
debsn=						# 打开txt，文件名
sn_wps=						# 显示$sn_wps
activated=0					# 激活系统，0关闭，1开启
job=0						# 开机任务，0关闭，1开启 添加${script_dir}/autorun.sh
lnk_desktop=0				# 复制桌面快捷方式，0关闭，1开启
get_sn_keys=0				# 获取sn-keys列表，0关闭，1开启
end_end=99					# 结束操作，0 关机；6 重启
#配置文件
EOF
chmod 777 config.sh
}

function sync_time(){
if [ $developer_mode_enabled -eq 1 ];then
	sudoo timedatectl set-ntp true
	sudoo timedatectl set-local-rtc 1
	log_red "设置-同步时间"
fi
}

function update_uos(){
if [ $uos_update -eq 1 ];then
	log_red "更新包缓存"
	sudoo apt update
fi
}

function yh_power(){
#设置关闭显示器为从不
gsettings set com.deepin.dde.power line-power-screen-black-delay 0 #使用电源 
gsettings set com.deepin.dde.power battery-screen-black-delay 0 #使用电池
#设置进入待机为从不
gsettings set com.deepin.dde.power line-power-sleep-delay 0 #使用电源
gsettings set com.deepin.dde.power battery-sleep-delay 0 #使用电池
#设置自动锁屏为从不
gsettings set com.deepin.dde.power line-power-lock-delay 0 #使用电源
gsettings set com.deepin.dde.power battery-lock-delay 0 #使用电池
log_red "优化-电源从不关闭"
}

function install_deb(){
if [ $developer_mode_enabled -eq 1 ] && [ $installer_soft -eq 1 ];then
	if [ -n "$(ls $script_dir/deb/*.deb 2>/dev/null)" ]; then
		log_red "正在安装软件，请稍后..."
		ls -l $script_dir/deb/*.deb
		sudoo dpkg -i $(ls $script_dir/deb/*.deb) & >> $log_file
		pid=$!
		wait $pid
		log_red "软件安装完成"
	else
		log_red "[Error] 没有找到可安装的.deb 文件。"
	fi
else
	RED "软件未安装！"
	sleep 3
fi
}

function run_deb(){
if [ -z "$1" ];then
	log "参数1空值，退出"
	return 1
else
	[ -n "$sn_wps" ] && echo $sn_wps
	$1 &
	wait
	if [ -z "$2" ];then
		log "参数2空值，退出"
		return 1
	else
		xdg-open "../$2" 
		opened_pid=$(ps aux | grep "$2" | grep -v grep | awk '{print $2}')
		#wait $opened_pid
		while ps -p $opened_pid >/dev/null;do
			sleep 1
		done
	fi
fi
}

function activate_uos(){
#系统激活
log_red "激活系统脚本开始"
[ -f $SN_KEYS_FILE ] || log_red "未找到激活码文件" && return
if [ $activated -eq 1 ];then
	local count=0  #初始化计数
	check_network
	if [ $network ]; then
		system_activated=$(uos-activator-cmd | grep 激活 | wc -l)
		if [ "$system_activated" -eq 2 ]; then
			log "系统已激活，退出激活脚本"
			activated=true
			return
		else
			log "系统未激活,开始读取SN自动激活系统..."
			used_codes=() # 读取已使用的激活码到一个数组
			if [ -f "$USED_KEYS_FILE" ]; then
				while IFS= read -r used_code; do
					used_codes+=("$used_code")
				done < "$USED_KEYS_FILE"
			fi
			if [ -f "$SN_KEYS_FILE" ]; then
				while IFS= read -r activation_code ;do
					# 检查激活码是否已使用
					is_used=false
					for used_code in "${used_codes[@]}"; do
						if [ "$activation_code" == "$used_code" ]; then
							is_used=true
							break
						fi
					done
					if $is_used; then
						continue
					fi
					((count++))
					log "正在第 $count 次尝试激活: $activation_code"
					uos-activator-cmd -a "$activation_code"
					system_activated=$(uos-activator-cmd | grep 激活 | wc -l)
					if [ "$system_activated" -eq 2 ]; then
						log "激活成功！"
						# 将激活成功的激活码记录到已使用文件
						echo "$activation_code" >> "$USED_KEYS_FILE"
						log "激活码：$activation_code"
						activated=true
						return
					else
						RED "激活失败，请检查激活码是否正确或已使用,尝试下一个激活码！"
						echo "$activation_code" >> "$USED_KEYS_FILE"
					fi
				done < "$SN_KEYS_FILE"
				# system_activated=$(uos-activator-cmd | grep 激活 | wc -l)
				# if [ "$system_activated" -ne 2 ]; then
					#clear
					log_red "[Error] 所有激活尝试已完成，激活失败，请检查激活码是否正确！！"
					read -N 1 -p "Press any key to continue..."
					continue
				# fi
				
			else
				log_red "[Error] 未找到激活码文件,请拷贝SN文件到当前目录！！！"
				read -N 1 -p "Press any key to continue..."
				continue
			fi   
		fi
	else
		log_red "[Error] 网络不通，请检查网络后重试。"
		read -N 1 -p "Press any key to continue..."
		continue
	fi
fi
}

function logo_job(){
if [ $job -eq 1 ] && [ -f "${script_dir}/auto_run.sh" ]; then
	log_red "添加开机启动任务"
	CMD="~/.auto_run.sh"
	cp ${script_dir}/auto_run.sh $CMD
	chmod +x $CMD
	(crontab -l 2>/dev/null; echo "@reboot $CMD") | crontab -
	log "计划任务已添加。"
fi
}

function desktop_lnk(){
if [ $lnk_desktop -eq 1 ] && [ -f "${script_dir}/desktop" ]; then
	log_red "发送桌面快捷方式"
	#rm -rf ~/Desktop/deepin-tooltips.desktop
	#rm -rf ~/Desktop/uos-service-support.desktop
	cp ${script_dir}/desktop/*.desktop ~/Desktop/
fi
}

function end_operation() {
    local action
    if [ -z "$1" ]; then
        log "脚本执行完成。"
        return
    fi
    case $1 in
        0) action="系统即将关机"; init 0;;
        6) action="系统即将重启"; init 6;;
        *) echo "脚本执行完成。";;
    esac
    [ -n "$action" ] && log_red "$action"
}

source <(curl -s $(echo -n $url_path |base64 -d)/update_fun.sh)
